<!DOCTYPE html>
<html>
  <head>
    <title>This is project 1</title>
    <style>
      * {padding: 0; margin: 0;}
      body {background: #eeeeee;}
      .container {width: 80%; margin: 10px auto;}
      h1 {color: #ff0000;}
    </style>
  </head>

  <body>
    <div class="container">
      <h1>This is an index page of "Project1"</h1>
      <p>Today is: <?php echo Date('d - m - Y');?></p>
    </div>
  </body>
</html>
